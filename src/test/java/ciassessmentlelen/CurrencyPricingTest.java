/*Lelen Abeywardena*/

package ciassessmentlelen;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.*;

import java.io.File;

import org.junit.Test;

public class CurrencyPricingTest {
	
	ClassLoader classLoader = getClass().getClassLoader();

	@Test
	public void quoteTest_AEDDKK_onratescsv_shouldreturn15209() {
		
		
		File file = new File(classLoader.getResource("rates.csv").getFile());
		CurrencyPricing exchange = new CurrencyPricing(file.getAbsolutePath());
		try {
			assertEquals(1.5209, exchange.getFXQuote("AED", "DKK"), 0.0001);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	@Test
	public void quoteTest_DJFNOK_asminorpair_shouldreturn075() {
		
		
		File file = new File(classLoader.getResource("minorpair.csv").getFile());
		CurrencyPricing exchange = new CurrencyPricing(file.getAbsolutePath());
		try {
			assertEquals(0.75, exchange.getFXQuote("DJF", "NOK"), 0.0001);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	
	
	@Test
	public void quoteTest_DJFNOK_shouldthrowexception() {
		
		File file = new File(classLoader.getResource("rates.csv").getFile());
		CurrencyPricing exchange = new CurrencyPricing(file.getAbsolutePath());
		try {
			exchange.getFXQuote("DJF", "NOK");
			fail("expected Exception to be thrown");
		} catch (Exception e) {
			// TODO Auto-generated catch block
			assertThat(e.getMessage(), is("Curency pairing cannot be priced"));
			
		}
	}

}
