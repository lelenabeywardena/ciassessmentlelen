/*Lelen Abeywardena*/

package ciassessmentlelen;

import static org.hamcrest.CoreMatchers.*;
import static org.junit.Assert.*;

import java.io.File;

import org.junit.Test;

public class CSVReaderTest {

	@Test
	public void test() {
		ClassLoader classLoader = getClass().getClassLoader();
		File file = new File(classLoader.getResource("rates.csv").getFile());
		
		CSVReader reader = new CSVReader(file.getAbsolutePath(), ",");
		assertThat(reader.currencyList.size(), not(0));
	}

}
