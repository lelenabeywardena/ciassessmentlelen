/*Lelen Abeywardena*/

package ciassessmentlelen;

//https://www.mkyong.com/java/how-to-read-and-parse-csv-file-in-java/

import java.io.BufferedReader;
import java.io.FileReader;


import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class CSVReader {
	
	public List<List<String>> currencyList = new ArrayList<List<String>>();

    public CSVReader(String csvFile, String split) {

        String line = "";
        
        
        try (BufferedReader br = new BufferedReader(new FileReader(csvFile))) {

            while ((line = br.readLine()) != null) {

                String[] linePieces = line.split(split);

                List<String> csvPieces = new ArrayList<String>(linePieces.length);
                for(String piece : linePieces)
                {
                    csvPieces.add(piece);
                }
                currencyList.add(csvPieces);
            }

        } catch (IOException e) {
            e.printStackTrace();
        }

    }

}

