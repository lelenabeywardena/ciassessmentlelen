/*Lelen Abeywardena*/

package ciassessmentlelen;

public class CurrencyPricing {
	
	private CSVReader reader;

	public CurrencyPricing(String path) {
		
		reader = new CSVReader(path, ",");
		
	}
	
	public double getFXQuote(String c1, String c2) throws Exception {
		//System.out.println(reader.currencyList);
		double mostCurrent = -1;
		for (int x = 0; x < reader.currencyList.size(); x++) {
			if(reader.currencyList.get(x).get(0).equals(c1) && 
					reader.currencyList.get(x).get(1).equals(c2)) {
				mostCurrent = Double.parseDouble((reader.currencyList.get(x).get(2)));
			}
		}
		if(mostCurrent < 0 && !(c2.equals("USD")|c1.equals("USD"))) {
			mostCurrent = getFXQuote(c1, "USD")*getFXQuote("USD", c2);
		}
		if(mostCurrent < 0) {
			throw new Exception("Curency pairing cannot be priced");
		}
		return mostCurrent;
	}
}
